import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

Row _buildPersonalDetail(IconData icon, String head, String detail) {
  return Row(
    mainAxisSize: MainAxisSize.min,
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: [
      Icon(
        icon,
        color: Colors.black,
      ),
      Container(
        padding: const EdgeInsets.only(left: 10),
        child: Text(
          head,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 24,
          ),
        ),
      ),
      Expanded(
          child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            padding: EdgeInsets.only(left: 0),
            child: Text(
              detail,
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
        ],
      )),
    ],
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
        padding: const EdgeInsets.all(32),
        child: Column(
          children: [
            Container(
              child: _buildPersonalDetail(
                  Icons.account_box, 'ชื่อ:', 'ธนมล บุญช่วย'),
            ),
            Container(
              child: _buildPersonalDetail(
                  Icons.cake, 'วันเกิด:', '17 เมษายน 2543'),
            ),
            Container(
              child: _buildPersonalDetail(Icons.male, 'เพศ:', 'หญิง'),
            ),
            Container(
              child: _buildPersonalDetail(Icons.home, 'ที่อยู่:',
                  '49/2 ม.8 ต.บางปะกง อ.บางปะกง จ.ฉะเชิงเทรา'),
            ),
            Container(
              child: _buildPersonalDetail(
                  Icons.email, 'Email:', 'pop.tanamon@gmail.com'),
            ),
            Container(
              child: _buildPersonalDetail(
                  Icons.phone, 'เบอร์โทร:', '095-846-4412'),
            ),
          ],
        ));
    Widget skillSection = Container(
      padding: const EdgeInsets.fromLTRB(32, 0, 0, 0),
      child: _buildPersonalDetail(Icons.computer, 'ทักษะ', ''),
    );
    Widget imageListSection = Container(
        padding: const EdgeInsets.fromLTRB(0, 0, 0, 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            new Container(
              width: 100,
              height: 100,
              decoration: new BoxDecoration(
                  image: new DecorationImage(
                fit: BoxFit.fitHeight,
                image: new AssetImage('img/Java.jpg'),
              )),
            ),
            new Container(
              width: 100,
              height: 100,
              decoration: new BoxDecoration(
                  image: new DecorationImage(
                fit: BoxFit.fitHeight,
                image: new AssetImage('img/JavaScript.png'),
              )),
            ),
            new Container(
              width: 100,
              height: 100,
              decoration: new BoxDecoration(
                  image: new DecorationImage(
                fit: BoxFit.fitHeight,
                image: new AssetImage('img/Python.png'),
              )),
            ),
            new Container(
              width: 100,
              height: 100,
              decoration: new BoxDecoration(
                  image: new DecorationImage(
                fit: BoxFit.fitHeight,
                image: new AssetImage('img/NodeJS.png'),
              )),
            ),
            new Container(
              width: 100,
              height: 100,
              decoration: new BoxDecoration(
                  image: new DecorationImage(
                fit: BoxFit.fitHeight,
                image: new AssetImage('img/VueJS.png'),
              )),
            ),
          ],
        ));
    Widget educationSection = Container(
      padding: const EdgeInsets.fromLTRB(32, 0, 0, 10),
      child: _buildPersonalDetail(Icons.school, 'การศึกษา', ''),
    );
    Widget educationDataSection = Container(
        padding: const EdgeInsets.fromLTRB(32, 0, 32, 0),
        child: Column(
          children: [
            Container(
              child: _buildPersonalDetail(
                  Icons.groups, 'การศึกษาระดับปริญญาตรี:', 'มหาวิทยาลัยบูรพา'),
            ),
            Container(
              child: _buildPersonalDetail(
                  Icons.groups, 'การศึกษาระดับมัธยมศึกษา:', 'บางปะกง บวรวิทยายน'),
            )
          ],
        ));
    return MaterialApp(
        title: 'My Resume',
        home: Scaffold(
          body: ListView(
            children: <Widget>[
              new Container(
                width: 250,
                height: 250,
                decoration: new BoxDecoration(
                    shape: BoxShape.circle,
                    image: new DecorationImage(
                      fit: BoxFit.fitHeight,
                      image: new AssetImage('img/Tanamon.jpg'),
                    )),
              ),
              titleSection,
              skillSection,
              imageListSection,
              educationSection,
              educationDataSection
            ],
          ),
        ));
  }
}
